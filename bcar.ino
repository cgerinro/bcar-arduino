const int motor1_enablePin = 11; //pwm
const int motor1_in1Pin = 13;
const int motor1_in2Pin = 12;
 
const int motor2_enablePin = 6; //pwm
const int motor2_in1Pin = 8;
const int motor2_in2Pin = 7;

const int liaisonSerie = 9600;

void setup()
{
  //on initialise les pins du moteur 1
  pinMode(motor1_in1Pin, OUTPUT);
  pinMode(motor1_in2Pin, OUTPUT);
  pinMode(motor1_enablePin, OUTPUT);
 
  //on initialise les pins du moteur 2
  pinMode(motor2_in1Pin, OUTPUT);
  pinMode(motor2_in2Pin, OUTPUT);
  pinMode(motor2_enablePin, OUTPUT);

  Serial.begin(liaisonSerie); // Initialisation de la liaison série à 115200 bps

}
 
void loop()
{
  while (Serial.available() == 0);
  char c = Serial.read();

   if(c =='F') { 
      SetMotor2(250, false);
      SetMotor1(250, false);
   }
   else if(c == 'L'){
      SetMotor2(250, true);
      SetMotor1(250, false);
   }
   else if(c == 'R'){
      SetMotor2(250, false);
      SetMotor1(250, true);
   }
   else if(c == 'B'){
      SetMotor2(250, true);
      SetMotor1(250, true);
   }
   else if( c == 'S'){
      SetMotor2(0, true);
      SetMotor1(0, true);
   }
 
}
 
//Fonction qui set le moteur1
void SetMotor1(int speed, boolean reverse)
{
  analogWrite(motor1_enablePin, speed);
  digitalWrite(motor1_in1Pin, ! reverse);
  digitalWrite(motor1_in2Pin, reverse);
}
 
//Fonction qui set le moteur2
void SetMotor2(int speed, boolean reverse)
{
  analogWrite(motor2_enablePin, speed);
  digitalWrite(motor2_in1Pin, ! reverse);
  digitalWrite(motor2_in2Pin, reverse);
}
